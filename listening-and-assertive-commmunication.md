# Listening and Assertive Communication

**Question 1 : What are the steps/strategies to do Active Listening?**

**Answer :**

Strategies to do active listening:
1. Avoid getting distracted by your own thoughts.
2. Focus on the speaker and the topic.
3. Try not to interrupt the other person.
4. Let them finish and then respond.
5. Use door openers.
6. If appropriate, take notes about important points in conversation.
7. Paraphrase what others have said to make sure, you both are on the same page.

---

**Question 2 : According to Fisher's model, what are the key points of Reflective Listening?**

**Answer :**

1. Understanding feelings of the speaker.
2. Repeating back important points in your own words.
3. Showing you're listening by paying attention, eye contact and body language.
4. Making them feel heard and understanding what they are trying to say.
5. No judging or giving advice to fix it. 

---

**Question 3 : What are the obstacles in your listening process?**

**Answer :**

Obstacles in my listening process:
1. Get easily distracted by my own thoughts.
2. Lose focus if speaker is monotonous.
3. Very commonly interrupt others to make my point.
4. Can't control changing mood while listening.

---

**Question 4 : What can you do to improve your listening?**

**Answer :**

To improve listening:
1. Will try to focus more on the speaker and the topic.
2. Will wait for speaker's speech to complete, to add my input.
3. Will keep conversations open for anyone's input.
4. To focus more, will take notes while listening in important topics.

---


**Question 5 : When do you switch to Passive communication style in your day to day life?**

**Answer :**

1. When I feel confused, and not able to take decision.
2. When I don't have enough energy to handle confrontation or conflicts.
3. When I feel, I'll lose money or relationship, if I talk assertively.

---

**Question 6 : When do you switch into Aggressive communication styles in your day to day life?**

**Answer :**

1. When I am frustrated / tired, and sill getting disturbed by someone.
2. When I know, I'm correct and still been proven wrong.
3. When I feel, there are no repercussions of being aggressive.
4. When I stop caring about relation or outcome.
   
---

**Question 7 : When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**

**Answer :**

1. When I become comfortable around people.
2. When I feel need for Attention in life.
3. When I feel insecure about my own feelings.
4. When I want to assert control on others.

---

**Question 8 : How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?**

**Answer :**

1. I'll speak to the point, and will avoid vague language or hints.
2. I'll practice active listening to get better clarity of speaker's thoughts.
3. I'll learn to say no to those things which don't feel correct to me.
4. I'll try to improve my body language, straight back, direct eye contact.
5. I'll keep my tone confident, speak clearly and avoid any misunderstanding.
   





---