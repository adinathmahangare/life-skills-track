# Prevention of Sexual Harrassment

**Question 1: What kinds of behaviour cause sexual harassment?**

**Answer:**

There are three forms of sexual harassment behavior: verbal, visual and physical

1. **Verbal harassment includes:**
  
   * Comments about clothing, a person's body, sexual or gender based jokes or remarks.
   * Requesting sexual favors or repeatedly asking someone out.
   * Sexual innuendos, threats, spreading rumors about a person's personal or sexual life, or using foul and obscene language.

2. **Visual harassment can include:**
        
   * Posters, drawings, pictures, screen savers, cartoons, emails or texts of a sexual nature.
  
3. **Physical harassment often includes:**

   * Sexual assault 
   * impeding or blocking movement
   * inappropriate touching
   * sexual gesturing
   * leering or staring
---

**Question 2: What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

**Answer:**

* **If you're the target:** Clearly tell the person to stop. Report it to a supervisor, HR, or a trusted colleague.
  
* **If you witness it:** Support the target by asking if they're okay. Report it to a trusted authority figure yourself.


---


