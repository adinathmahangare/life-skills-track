# Growth and Grit Mindset

### Question 1: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

**Answer:** According to the video, having a growth mindset is the belief that your ability to learn is not fixed, and it can improve with effort. People with a growth mindset are more likely to persevere when they fail because they believe that failure is not a permanent condition.

### Question 2: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

**Answer:** According to the video, a growth mindset is the belief that your abilities can improve with effort, while a fixed mindset believes your abilities are set. People with a growth mindset embrace challenges and see mistakes as opportunities to learn.

### Question 3: What is the Internal Locus of Control? What is the key point in the video?

**Answer:** According to the video, the key point is that having an internal locus of control is the key to staying motivated.

* The video talks about a study conducted in 1998 at Columbia University by a professor Claudia M Mueller. In the study, a large group of fifth graders were asked to work on numerous puzzles by themselves. All the children were told that they scored very well on the puzzles regardless of how well they actually did. Then half of the students were told that they scored high because they were smart and gifted, while the other half were told that it was because they worked hard.  The researchers then presented each student with three more types of puzzles to work on: easy ones, medium difficulty ones, and extremely challenging ones.

* The results showed that the students who were told that they did well because they were smart spent most of their time on the easy puzzles and avoided spending time on the challenging ones. They also spent less time overall trying to solve any of the puzzles. In addition, they reported that they did not enjoy the experiment. On the other hand, the students who were told that they did well because they worked hard spent most of their time focused on the harder puzzles and spent more time overall trying to solve the puzzles. They also reported that they enjoyed the entire experience.

* This experiment shows that having an internal locus of control, which means believing that you have control over your life, is important for motivation. People who believe that their success is due to their own effort are more likely to be motivated to keep trying.  The video also talks about the importance of taking responsibility for your actions and solving problems in your own life. When you take steps to solve a problem and you see improvement, it reinforces the idea that you are in control. This can help you develop an internal locus of control and increase your motivation.

### Question 4: What are the key points mentioned by speaker to build growth mindset (explanation not needed).

**Answer:**According to the speaker, here are the key points to build growth mindset:

* Believe in your ability to figure things out.
* Question your assumptions.
* Develop your own life curriculum.
* Honor the struggle. 

### Question 5: What are your ideas to take action and build Growth Mindset?

**Answer:**
* Believe in your ability to figure things out.
* Question your assumptions. Don't limit yourself by believing negative thoughts.
* Develop your own life curriculum. Take charge of your own learning.
* Honor the struggle. View difficulty as a learning experience.
* Have an internal locus of control. Believe you have control over your life.
* Take action and solve problems in your own life.
* Make changes and attribute improvement to your effort.