# Why OOP?

Before learning any concept of object-oriented programming, I was also puzzled, what's the point of using OOP in your programs. And after watching numerous YouTube tutorial videos, solving hundreds of leetcode questions, and working on multiple projects in the last five years, I came to the conclusion, you must implement Object-Oriented Programming in your programming, because of its modularity and reusability. Let's dive into these two concepts individually.


## Modularity

Have you ever experienced living in a hostel room before? If you have, then you must have memories about the condition of your room! All possible things like snacks packets, books, laptop, dishes, medicines, water bottles chaotically sitting on your table, all the clothes heaped on your bed, or in your cupboard. So, basically if you lost anything, it'll be lost forever. What you needed was proper organization of all the into different compartments. All the study material on your table, all the different type clothes folded and put in different parts of cupboard. So, whenever you feel you lost something, you just need to check that specific compartment. And now you can have good idea about, what is modularity? So, the modularity provides organization to your code, store different use-cases into different modules. And while testing the codebase, whenever you find any error, you'll know exactly which module to look for. This modularity in programming helps a lot to the new employee joining in the place of old employee, to understand the incomplete code written. Now let's move on to the concept of reusability.


## Reusability

While programming any full stack project, there always are some parts of code which repeats itself again and again. Suppose, you are creating an e-commerce website, in the interface all the products are fitted in almost same formatting, just product related information might be different. So, to create this type of interface, you don't have to write the same code again and again for each product. Object-oriented programming provides you classes, which creates a template for product description, which can be reused to create any number of product blocks. Core concepts of object-oriented programming like inheritance and encapsulation also supports the reusability a lot.
 
# Pillars of OOPs

Now, let's try to understand what makes object-oriented programming so special and different from Procedural programming and meta programming. We are going to see each pillar of object-oriented programming, with one example for each one.


### Encapsulation

Encapsulation means binding data and methods of an any real thing in the form of object. Whenever we use a class to create an object, it forms an imaginary capsule which contains all the information about the object inside it. Access specifiers public, private and protected are used to define which properties of data are modifiable outside the function. Correct way of applying encapsulation is using private access specifier for variables and public for functions.

``` cpp
#include <iostream>
using namespace std;

class dog {
    private: 
        string name;
        string color;

    public:

        void getName(){
            cout << name;
        }

        void getColor(){
            cout << color;
        }

};

int main() {
    dog Tommy;
    return 0;   
}
```


### Abstraction 

When you go to a restaurant, you are not allowed to see how food is made, you are just served with food. In programming, abstraction is similar. It's about hiding the complex details of how something works and focusing on what the user needs to know to use it. In the following example, The Light class hides the complex details of how the light works and exposes simple functions like turnOn and turnOff for the user (the program) to interact with. This makes the code easier to understand and use.

```cpp
#include <iostream>
using namespace std;

class Light {
    public:
    void turnOn() {
        // Code to actually turn on the light (hidden)
        cout << "Light turned on!" << endl;
    }

    void turnOff() {
        // Code to actually turn off the light (hidden)
        cout << "Light turned off!" << endl;
    }
};

int main() {

    Light bulb;
    bulb.turnOn();      //output Light turned on!
    bulb.turnOff();     //output Light turned off!
    return 0;
}
```


### Inheritance

Inheritance in OOP is like a family tree, where children inherit traits and characteristics from their parents. In programming, it allows you to create new classes (children) that reuse properties and behaviors from existing classes (parents). In the following example, Car inherits color and speed from Vehicle and adds its own property wheels and method openTrunk. Bike also inherits color and speed but adds hasKickstand and ringBell.

```cpp
#include <iostream>
using namespace std;

class Vehicle {
    public:
    string color;
    int speed;

    void accelerate() {
        speed++;
    }

    void brake() {
        speed--;
    }
};

class Car : public Vehicle { 
    public:
    int wheels;

    void openTrunk() {
        cout << "Trunk opened!" << endl;
    }
};

class Bike : public Vehicle { 
    public:
    bool hasKickstand;

    void ringBell() {
        cout << "Ring ring!" << endl;
    }
};

int main() {

    Car myCar;
    myCar.color = "Red";
    myCar.wheels = 4;

    Bike myBike;
    myBike.color = "Blue";
    myBike.hasKickstand = true;

    myCar.accelerate(); // Red vehicle accelerated...
    myCar.openTrunk();  // Red car's trunk opened!

    myBike.brake();    // Blue vehicle braked... (inherited from Vehicle)
    myBike.ringBell(); // Blue bike rings its bell! Ring ring!
    
    return 0;
}
```

### Polymorphism 

Suppose you created one class animal, which contains all the basic things animals can do. And then completed its child classes as different class for every animal. But every animal have different voice. So calling values of voice vaiable returns different output for different animals. OOP provides ability for objects of different classes, to respond differently to same message. And this ability is known as polymorphism. There are two types of polymorphism.

1. Runtime Polymorphism: In this example, even though we call makeSound through animal pointers, the specific Dog::makeSound or Cat::makeSound function gets called at runtime depending on the actual object being pointed to. This demonstrates runtime polymorphism.
   
```cpp
#include <iostream>
using namespace std;

class Animal {
    public:
    virtual void makeSound() {
        cout << "Generic animal sound" << endl;
    }
};

class Dog : public Animal {
    public:
    void makeSound() {
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal {
    public:
    void makeSound() {
        cout << "Meow!" << endl;
    }
};

int main() {

    Dog Tommy;
    Cat Manu;

    Tommy.makeSound();  //output Woof!
    Manu.makeSound();   //output Meow!

    return 0;
}
```


2. Compile time Polymorpism:  In this example, the compiler determines which print function to call based on the type of the argument passed at compile time. If you call print(num) it will use the first function, and print(str) will use the second function.

```cpp
#include <iostream>
using namespace std;

void print(int value) {
    cout << "Integer: " << value << endl;
}

void print(string value) {
    cout << "String: " << value << endl;
}


int main() {

    int num = 4;
    string str = "Str";

    print(num);        //output Integer: 4
    print(str);        //output String: str
    return 0;
}
```


# Summary

This blog post argues that Object-Oriented Programming (OOP) is beneficial because of its modularity and reusability. Modularity is like organizing your room so you can find things easily. In programming, it means separating your code into modules that handle specific tasks. Reusability means being able to use the same code for different purposes. OOP allows you to create templates (classes) that can be reused to create many objects. The post also briefly explains the four main pillars of OOP: encapsulation, abstraction, inheritance, and polymorphism.


  

## References
* https://stackoverflow.com/questions/24270/whats-the-point-of-oop
  
* https://www.codecademy.com/article/cpp-object-oriented-programming
  
* https://youtu.be/OQ0En1dQ3zI?si=CW5NqeqEh3CqMeeA
  
* https://www.geeksforgeeks.org/object-oriented-programming-in-cpp/
