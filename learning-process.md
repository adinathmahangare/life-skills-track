# Learning Process

**Question 1 : What is the Feynman Technigue? Explain in 1 line.**

**Answer**: A good way to check if you have learned something is to explain it to someone in a simple manner.

---

**Question 2 : In this video, what was the most interesting story or idea for you?**

**Answer**: The interesting thing I learned is that our brains learn in two ways: focus mode and chill mode. In focus mode, you concentrate hard on a specific topic. Chill mode is when you relax, and that's when your brain actually connects all the information you learned earlier. The video says that switching between these two modes is the best way to really learn something new!

---

**Question 3 : What are active and diffused modes of thinking?**

**Answer**: Two modes of thinking are, 

* Diffused thinking: Relaxed way thinking about random things to explore new ideas in brain.
* Active thinking: Concentrated thinking about a specific topic .

---

**Question 4 : According to the video, what are the steps to take when approaching a new topic? Only mention the points.**

**Answer**: Steps to take when approaching new topic are,

1. Break down learning goal into smaller feasible parts.
2. Learn enough part of the skill, so you can start working on it yourself and self correct.
3. Remove the distractions which are coming between learning process.
4. Give 20 hours to the process of learning without giving up.

---

**Question 5 : What are some of the actions you can take going forward to improve your learning process?**

**Answer**: Actions I will take to improve my learning process are,

1. Learn one thing at a time, and put all the focus in it.
2. Will not stuck in a completion trap, it's not about just completing the task, but understanding the process.
3. Will focus more on practicing rather than watching endless tutorials.

---