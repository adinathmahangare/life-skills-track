**Question 1: In this video, what was the most interesting story or idea for you?**

**Answer:**
In this video, the most interesting idea is that creating long-term behavior change is possible by starting with tiny habits. The speaker argues that relying on motivation and willpower is not an effective way to change behavior in the long term. Instead, he proposes a method centered around creating tiny habits that are easy to repeat and can be incorporated into existing routines.


**Question 2: How can you use B = MAP to make new habits easier? What are M, A and P.**

**Answer:**
* B : Behaivior  
* M : Motivation
* A : Ability
* P : Prompt

B = MAP

If you are trying to build a new habit you only start performing act of habit, if you have enough motivation, energy or ability to complete the task and availability of the promt to perform the task.


**Question 3: Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)**

**Answer:**

Celebrating even the smallest wins is the most critical component of habit development. Relying on motivation alone to develop a habit is not a reliable strategy. Celebrating tiny wins reinforces the habit loop, making you more likely to repeat the behavior. Celebrating small wins builds success momentum, which is more important than the size of the wins themselves. Celebrating tiny completions increases your confidence and motivation to repeat the behavior, allowing you to gradually increase the difficulty of the habit over time.


**Question 4: In this video, what was the most interesting story or idea for you?**

**Answer:**
In this video, the most interesting story is about Dave Brailsford and his philosophy of marginal gains. Brailsford completely transformed British Cycling from a mediocre team to a a dominant one. His strategy was to focus on small improvements in every possible area.  For example, they made the riders wear lighter tires and more aerodynamic suits. They even experimented with different massage gels to see which reduced muscle recovery time the most.  These tiny gains added up to a significant improvement in performance. The video argues that small improvements can lead to big results over time. This is because these improvements compound, meaning that the benefits of each improvement build on top of the ones before. The speaker of the video, James Clear, says that small improvements are the way to achieve lasting change.

**Question 5: What is the book's perspective about Identity?**

**Answer:**
Instead of focusing on achieving a specific outcome, we should focus on who we want to be. Our habits should reflect the person we view ourselves as. For instance, if you want to be a healthy person, you would develop habits that a healthy person would do, such as eating healthy foods and exercising regularly.  This approach is more sustainable in the long term than simply trying to achieve a specific outcome, such as losing weight.

**Question 6: Write about the book's perspective on how to make a habit easier to do?**

**Answer:**
* **Small changes add up:** Forget drastic goals, focus on tiny, easy habits you can repeat.
* **Make it easy & attractive:** Design your environment for success. Flossing next to your brush? Yes! Rewards like podcasts make it more fun.
* **Identity over outcome:** Don't just chase a goal, be the person who achieves it. Want to be healthy? Build healthy habits.


**Question 7: Write about the book's perspective on how to make a habit harder to do?**

**Answer:**
* **Make it invisible:** If you want to break the habit of checking your phone in the morning, put your phone away at night or turn it off.
* **Make it unattractive:** If you want to stop eating unhealthy snacks, get rid of them from your house.
* **Make it difficult:** If you want to go to the gym less, make it inconvenient to get there. You can try going to a gym that is farther away or one with less convenient hours.
* **Make it unsatisfying:** If you want to watch less TV, unplug your TV or get rid of your cable.

**Question 8: Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

**Answer:** I want to build new habit, daily workout for minimum 30 minutes
1. Make the cue obvious: Out of sight, out of mind! To trigger workout reminders:

    * Lay out your workout clothes the night before so they're the first thing you see in the morning.
    * Set workout reminders on your phone or a smart speaker with an upbeat tone to get you pumped.
    * Place workout equipment like yoga mats or free weights in a visible location to serve as a constant cue.

2. Make it attractive: Who says exercise can't be fun? Here's how to make workouts more appealing:
    * Find workout routines you actually enjoy - dancing, cycling, following fitness channels online.
    * Listen to a motivating playlist or podcast during your workout.
    * Work out with a friend or join a fitness class for an added social element.

3. Make it easy: Friction is the enemy of habit formation. Let's minimize it:
    * Choose quick, 30-minute workouts you can do at home with minimal equipment if needed.
    * Prep any workout essentials like water bottles or workout snacks the night before.
    * Schedule your workouts in your calendar and treat them like any other important appointment.

4. Make the response satisfying: The post-workout feeling is key! Let's make it rewarding:
    * Track your progress in a journal or app to see your improvement and celebrate milestones.
    * Reward yourself after workouts with a healthy snack or a relaxing activity you enjoy.
    * Focus on the positive benefits you feel after exercise - more energy, better mood, etc.


**Question 9: Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

**Answer:** I want to reduce my smartphone overuse:
1. Make the cue invisible: My constant phone use is probably triggered by seeing it everywhere. I can:

    * Put my phone away at night in another room to avoid the morning check.
    * Turn off notifications for non-essential apps to avoid the constant dings and buzzes.
    * Use a grayscale phone theme to make it visually less appealing.

2. Make the process difficult: Reaching for my phone is too easy! I can:

    * Set specific times for checking social media or news, and only use the phone during those designated slots.
    * Delete time-wasting apps or move them to a hidden folder to create an extra hurdle.
    * Keep my phone on silent mode most of the time to reduce the urge to unlock it for every notification.

3. Make the response unsatisfying: Scrolling mindlessly isn't fulfilling. I can:

    * Set time limits on specific apps to limit the mindless scrolling rabbit hole.
    * Reward myself with non-phone activities after completing a task, like taking a walk or reading a book.
    * Reflect on how I feel after excessive phone use. Does it leave me drained or unproductive?
